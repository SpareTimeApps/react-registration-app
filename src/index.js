
import "@babel/polyfill";
import React, {Component} from "react";
import promise from 'redux-promise';
import {createStore, applyMiddleware} from "redux";
import {Provider} from "react-redux";
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import LoginPanel from './js/components/loginPanel';
import allReducers from "./js/reducers";
import App from "./js/components/app";
import NavInstance from './js/containers/myNavInstance';
import LoginSuccess from './js/components/loginSuccessLanding'
import About from './js/components/about'
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import  RegistrationPanel  from './js/components/registrationPanel'
import RegSuccess from './js/components/RegSuccess'
import './scss/style.scss';


const createStoreWithMiddleware = (createStore(allReducers, applyMiddleware(promise)));

//const createStoreWithMiddleware = (createStore(allReducers));



ReactDOM.render((
    <Provider store={createStoreWithMiddleware}>
        <BrowserRouter>
            <div>
                <Switch>

                    <Route path="/login" component={LoginPanel}/>
                    <Route path="/loginsuccess" component={LoginSuccess}/>
                    <Route path="/about" component={About} />
                    <Route path="/registration" component={RegistrationPanel} />
                    <Route path="/regSuccess" component={RegSuccess} />
                    <Route exact path="/" component={App}/>

                </Switch>
            </div>
        </BrowserRouter></Provider>

    ),
    document.getElementById('root')
);


/*


 const store = (createStore(allReducers));

 ReactDOM.render(
 <Provider store={store}>
 <App/>

 </Provider>, document.getElementById('root'));



 */
