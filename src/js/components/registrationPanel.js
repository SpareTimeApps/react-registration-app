/**
 * Created by kavitha on 12/07/17.
 */

import React, {Component} from "react";
import NavInstance from '../containers/myNavInstance';
import {Field, reduxForm} from 'redux-form';
import {Link} from 'react-router-dom';
import  {regAction} from '../actions';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
function renderFields(field) {
    const {meta: {touched, error}} = field;
    const errorClass = `form-group ${touched && error ? 'has-danger' : ''}`;
    return (
        <div className={errorClass}>
            <label>{ field.label} </label>
            <input
                type="text"
                {...field.input}
                className="form-control "/>
            {touched ? error : ''}
        </div>
    );
}


class RegistrationPanel extends Component {
    onSubmit(values) {
        console.log("onSubmit: " + values);
        this.props.regAction(values);
        this.props.history.push("/regSuccess");
    }
    render() {
console.log("inside reg panel");
        const {handleSubmit} = this.props;
        return (
            <div>
                <NavInstance />
                <div  >
                    <form className="center_div textcolor" onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
                        <div  >
                            <Field label="First Name" name="first" component={ renderFields }/>
                            <Field label="Last Name" name="last" component={renderFields }/>
                            <Field label="Preferred User name" name="userName" component={ renderFields }/>
                            <Field label="Password" name="password" component={renderFields }/>
                        </div>
                        <div className="  btn-toolbar center_div_btn">
                            <button type="submit" className="btn btn-lg  btn-primary">Login</button>

                            <Link to="/" className="btn btn-lg btn-danger">Cancel </Link>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.first) {
        errors.first = "First name is required";
    }
    if (!values.last) {
        errors.last = "Last name is required";
    }

    return errors;
}

const mapDispatchToProps = function (dispatch) {
    console.log("mapDispatchToProps: ");
    return bindActionCreators({
        regAction: regAction
    }, dispatch);
};


export default reduxForm({
    validate,
    form: 'registrationForm'
})(
    connect(null, mapDispatchToProps)
    (RegistrationPanel)
);
