import React from 'react';
import AppHeader from '../containers/app-header';
import NavInstance from '../containers/myNavInstance';

require("../../scss/style.scss");

const App = () => (
    <div>
        <NavInstance />
        <AppHeader/>
    </div>
  );
export default App;
