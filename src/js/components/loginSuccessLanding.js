
import React, {Component} from "react";
import {connect} from 'react-redux';
//import {user} from '../reducers/loggedUser'
import  {loginAttempt} from '../actions';
import NavInstance from '../containers/myNavInstance';
class LoginSuccess extends Component {
    render() {
        return (
            <div>
                <NavInstance />
                <div className="mycontainer">
                    <div id="jumbotron">
                        <div className="textcolor">
                            <h1 >Welcome!  {this.props.user[0]}</h1>
                            <h2>Please find your user details below</h2>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log("map state to props" + state.user);
    return {user: [state.loggedUser.userName, state.loggedUser.password]}
}

export default connect(mapStateToProps)(LoginSuccess);
