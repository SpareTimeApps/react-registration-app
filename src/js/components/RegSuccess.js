import {connect} from 'react-redux';
import React, {Component} from "react";
import NavInstance from '../containers/myNavInstance';
require("../../scss/style.scss");

class RegSuccess extends Component {
    render() {
        return (
            <div>
                <NavInstance />
                <div className="mycontainer">
                    <div id="jumbotron">
                        <div className="textcolor">
                            <h1 >Welcome!  {this.props.user[0]}</h1>
                            <h2>Please find your user details below</h2>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    console.log("map state to props" + state.user);
    return {user: [state.regUser.first, state.regUser.last, state.regUser.userName]}
}

export default connect(mapStateToProps)(RegSuccess);
