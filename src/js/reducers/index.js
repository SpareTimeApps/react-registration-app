import {reducer as formReducer} from 'redux-form';

import {combineReducers} from 'redux';

import loggedUser from "./loggedUser";

import regUser from "./registeredUsers";


const allReducers = combineReducers({

    loggedUser: loggedUser,
    regUser: regUser,
    form: formReducer

});

export default allReducers