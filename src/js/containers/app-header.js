import React, {Component} from "react";
import {Link} from 'react-router-dom';
import SplitText from 'react-pose-text';
import ReactDOM from 'react-dom';

const charPoses = {
    exit: {opacity: 0, y: 20},
    enter: {
        opacity: 1,
        y: 0,
        delay: ({ charIndex }) => charIndex * 80
    }
};

class appHeader extends Component {
    render() {
        return (
                <div>
                    <div className="mycontainer center_div ">
                        <div id="jumbotron">             
                            <h1 className = "textcolor">
                                <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>My application Lite
                                </SplitText>
                            </h1>
                            <h3 className="textcolor">
                                <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>This is my landing page</SplitText>
                            </h3>
                        </div>
                
                        <hr />
                
                        <Link to="/about" className="btn-lg btn-info ">Learn More </Link>
                
                    </div>
                </div>
                );
    }
}

export default (appHeader);
