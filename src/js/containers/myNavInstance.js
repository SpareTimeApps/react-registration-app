import React, {Component} from "react";
import logo from   "../images/logo.jpg";
import { LinkContainer } from 'react-router-bootstrap';
import {Nav, Navbar, NavItem}  from 'react-bootstrap';
import '../../scss/style.scss';
class navInstance extends Component {
    render() {
        return (
                <div className="mynav">
                   <Navbar bg="light" expand="lg">
                        <Navbar.Brand >
                            <a href="/"><img src={logo} className="App-logo myimg" alt="logo"></img></a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                        <Navbar.Collapse  id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <LinkContainer to="/about"> 
                                    <NavItem>About Us</NavItem>
                                </LinkContainer>
                            </Nav>
                            <Nav className="justify-content-end">
                                <LinkContainer to="/login">
                                    <NavItem className='addpadding' href="/login">Login</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/register">
                                    <NavItem href="/login">Register</NavItem>
                                </LinkContainer>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div>
                );

    }
}

export default (navInstance);
